﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Update_O4_Emps_from_Lawson
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataSet dsEmployees = new DataSet(),
                dsCross=new DataSet();
            StringBuilder sbSql = new StringBuilder();
            try
            {
                sbSql.Clear();
                sbSql.Append("SELECT FIRST_NAME AS first_name");
                sbSql.Append(", LAST_NAME AS last_name");
                sbSql.Append(", [NAME_SUFFIX]");
                sbSql.Append(", FICA_NBR AS Soc_Sec_Num");
                sbSql.Append(", COMPANY AS branch_id");
                sbSql.Append(", PROCESS_LEVEL AS DeptID");
                //sbSql.Append(", JOB_CODE AS DeptID");
                sbSql.Append(", EMPLOYEE AS employee_id");
                sbSql.Append(", EMPLOYEE AS timeclock_id");
                sbSql.Append(" FROM[dbo].[EMPLOYEE]");
                sbSql.Append(" WHERE COMPANY IN(100, 1000)");
                //  sbSql.Append(" AND SALARY_CLASS='H'");
                sbSql.Append(" AND EMP_STATUS='A'");
                sbSql.Append(" ORDER BY last_name,first_name");
                using (CodeSQL clsSql = new CodeSQL())
                {
                    dsEmployees = clsSql.CmdDataset(Properties.Settings.Default.conRx, sbSql.ToString());
                    if (dsEmployees.Tables.Count > 0 && dsEmployees.Tables[0].Rows.Count > 0)
                    {
                        clsSql.CmdNonQuery(Properties.Settings.Default.conMatt, "TRUNCATE TABLE O4_ZFI_RSS_Employees");
                        foreach (DataRow dRow in dsEmployees.Tables[0].Rows)
                        {
                            sbSql.Clear();
                            sbSql.Append("SELECT [X1_DATA_MAP]");
                            sbSql.Append(" ,[X1_MAP_A3]");
                            sbSql.Append(" ,[X1_MAP_A7]");
                            sbSql.Append(" FROM [dbo].[X1DATAMAP]");
                            sbSql.Append(" WHERE X1_DATA_MAP='UNIVERSAL MAPPING'");
                            sbSql.Append($@" AND X1_MAP_A7={dRow["DeptID"]}");
                            dsCross = clsSql.CmdDataset(Properties.Settings.Default.conRx, sbSql.ToString());
                            sbSql.Clear();
                            sbSql.Append("INSERT INTO[dbo].[O4_ZFI_RSS_Employees] (");
                            sbSql.Append("[FirstName]");
                            sbSql.Append(",[LastName]");
                            sbSql.Append(",[SSN]");
                            sbSql.Append(",[BranchID]");
                            sbSql.Append(",[DeptID]");
                            sbSql.Append(",[EmpNum]");
                            sbSql.Append(",[EmpCode]");
                            sbSql.Append(") VALUES (");
                            sbSql.Append($@"'{dRow["first_name"].ToString().Trim()}'");
                            sbSql.Append($@",'{ dRow["last_name"].ToString().Trim()}'");
                            sbSql.Append($@",'{dRow["Soc_Sec_Num"].ToString().Trim().Replace("-", "")}'");
                            //sbSql.Append($@",{dRow["branch_id"].ToString().Trim()}");
                            sbSql.Append($@",{1}");
                            int iTemp = 0;
                            iTemp = int.TryParse(dRow["branch_id"].ToString(), out iTemp) ? iTemp : 0;
                            if (iTemp == 100)
                            {
                                sbSql.Append($@",{dsCross.Tables[0].Rows[0]["X1_MAP_A3"].ToString().Trim()}");
                            }
                            else
                            {
                                sbSql.Append($@",{1}");                        // RSS all Dept 1    6/28/2017
                                //sbSql.Append($@",{dRow["DeptID"]}");
                            }
                            sbSql.Append($@",{dRow["employee_id"].ToString().Trim()}");
                            sbSql.Append($@",'{dRow["timeclock_id"].ToString().Trim()}'");
                            sbSql.Append(")");
                            clsSql.CmdNonQuery(Properties.Settings.Default.conMatt, sbSql.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Application.Exit();
        }
    }
}
